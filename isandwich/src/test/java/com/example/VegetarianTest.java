package com.example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class VegetarianTest {
    
    @Test
    public void caesarSandwich_constructor_test() {
        try {
            Vegetarian v = new CaesarSandwich();
        }
        catch (UnsupportedOperationException e) {
            fail();
        }
    }

    @Test
    public void TofuSandwich_constructor_test() {
        try {
            Vegetarian v = new TofuSandwich();
        }
        catch (UnsupportedOperationException e) {
            fail();
        }
    }

    @Test
    public void caesarSandwich_addFilling_test() {
        Vegetarian v = new CaesarSandwich();
        v.addFilling("lettuce");
        assertEquals("caeser dressing lettuce ", v.getFilling());
    }

    @Test
    public void TofuSandwich_addFilling_test() {
        Vegetarian v = new TofuSandwich();
        v.addFilling("lettuce");
        assertEquals("Tofu lettuce ", v.getFilling());
    }

    @Test
    public void caesarSandwich_getFilling_test() {
        Vegetarian v = new CaesarSandwich();
        assertEquals("caeser dressing ", v.getFilling());
    }

    @Test
    public void TofuSandwich_getFilling_test() {
        Vegetarian v = new TofuSandwich();
        assertEquals("Tofu ", v.getFilling());
    }

    @Test
    public void caesarSandwich_isVegetarian_test_false() {
        Vegetarian v = new CaesarSandwich();
        assertEquals(false, v.isVegetarian());
    }

    @Test
    public void TofuSandwich_isVegetarian_test_true() {
        Vegetarian v = new TofuSandwich();
        v.addFilling("lettuce");
        assertEquals(true, v.isVegetarian());
    }

    @Test
    public void TofuSandwich_isVegetarian_test_false() {
        Vegetarian v = new TofuSandwich();
        v.addFilling("chicken");
        assertEquals(true, v.isVegetarian());
    }

    @Test
    public void TofuSandwich_isVegan_test_true() {
        Vegetarian v = new TofuSandwich();
        v.addFilling("lettuce");
        assertEquals(true, v.isVegan());
    }

    @Test
    public void TofuSandwich_isVegan_test_false() {
        Vegetarian v = new TofuSandwich();
        v.addFilling("cheese");
        assertEquals(false, v.isVegan());
    }
}

package com.example;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class BagelSandwichtest {
    @Test
    public void constructorTest(){
        try{
            BagelSandwich b = new BagelSandwich();
        }
        catch(UnsupportedOperationException e){
            fail();
        }
        
    }

    @Test
    public void addFillingTest(){
        BagelSandwich b = new BagelSandwich();
        b.addFilling("chicken");
        assertEquals("chicken", b.getFilling());
    }

    @Test
    public void getFillingTest(){
        BagelSandwich b = new BagelSandwich();
        assertEquals("", b.getFilling());
    }

    @Test
    public void isVegetarianTest(){
        BagelSandwich b = new BagelSandwich();
        try {
            b.isVegetarian();
        }
        catch(UnsupportedOperationException e) {
            assertTrue(true);
        }
   }
}

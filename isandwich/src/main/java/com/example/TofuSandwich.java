package com.example;

public class TofuSandwich extends Vegetarian{

    public TofuSandwich(){
        addFilling("Tofu");
    }
    @Override
    public String getProtein() {
        return "Tofu";
    }
    
}

package com.example;

public class CaesarSandwich extends Vegetarian
{

    public CaesarSandwich() {
        addFilling("caeser dressing");
    }

    @Override
    public String getProtein() 
    {
        return "anchovies";
    }

    @Override
    public boolean isVegetarian() {
        return false;
    }

}
package com.example;

public class App {
    public static void main(String[] args) {
        // Vegetarian v = new Vegetarian();
        ISandwich s = new TofuSandwich();
        TofuSandwich tofu = new TofuSandwich();

        if (tofu instanceof Vegetarian) {
            System.out.println("s is an instace of tofuSandwich");
        }

        Vegetarian vege = (Vegetarian) s;

        Vegetarian will = new CaesarSandwich();

        System.out.println(will.isVegetarian());
    }
}

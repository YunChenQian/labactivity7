package com.example;

public abstract class Vegetarian implements ISandwich{
    private String filling;
    public Vegetarian()
    {
        this.filling = "";
    }

    public abstract String getProtein();
    
    public void addFilling(String topping){
        String[] meats = new String[]{"chicken", "beef", "fish", "meat", "pork"};
        for (int i = 0; i < meats.length; i++){
            if (topping.equals(meats[i])){
                    throw new IllegalArgumentException();
            }
        }
        this.filling += topping + " ";
    }

    public String getFilling() {
        return this.filling;
    }

    public final boolean isVegetarian(){
        return true;
    }

    public boolean isVegan(){
        String[] dairy = new String[]{"cheese", "egg"};
        String[] split = this.filling.split("\\s+");
        for (String i : dairy){
            for(String j : split)
            {
                if(j.equals(i)){
                    return false;
                }
            }
        }
        return true;
    }
}
